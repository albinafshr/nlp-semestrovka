import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split

from sklearn.metrics import precision_recall_fscore_support


def get_word_ngram(x_train, x_test):
    vectorizer = TfidfVectorizer(max_features=50000, min_df=5, ngram_range=(3, 3))
    X_train_word_ngram = vectorizer.fit_transform(x_train)
    X_test_word_ngram = vectorizer.transform(x_test)

    return X_train_word_ngram, X_test_word_ngram


def get_score_model(model, X_test, y_test):
    y_pred = model.predict(X_test)
    metrics = precision_recall_fscore_support(y_pred=y_pred, y_true=y_test)
    print(f'Precision: {np.round(metrics[0], 3)}, recall: {np.round(metrics[1], 3)}, '
          f'f-measure: {np.round(metrics[2], 3)}')


def fit(x_train, y_train, x_test, y_test):
    model = LogisticRegression(solver="lbfgs")
    model.fit(x_train, y_train)
    get_score_model(model, x_test, y_test)


if __name__ == '__main__':
    df = pd.read_csv('lemmas.csv')
    X_train, X_test, y_train, y_test = train_test_split(df['review'], df['cat3'], test_size=0.2, shuffle=True)

    vectorizer = TfidfVectorizer(max_features=50000, min_df=5)
    X_train_vect = vectorizer.fit_transform(X_train)
    X_test_vect = vectorizer.transform(X_test)

    print('Word ngram')
    X_train_word_ngram, X_test_word_ngram = get_word_ngram(X_train, X_test)

    train_matrix = np.append(X_train_vect.toarray(), X_train_word_ngram.toarray(), axis=1)
    test_matrix = np.append(X_test_vect.toarray(), X_test_word_ngram.toarray(), axis=1)

    fit(train_matrix, y_train, test_matrix, y_test)
